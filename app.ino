#include <SCoop.h> //多工函式庫  https://reurl.cc/kd2d5d

#include <GP2Y1010AU0F.h> //PM2.5函式庫
#include <NewPing.h> //超聲波sensor函式庫

//蜂鳴器音調
#define Do  523
#define Re  587
#define Mi  659
#define Fa  698
#define So  784
#define La  880
#define Si  988
int ambulance[10] = {So, Si, So, Si, So, Si, So, Si, So, Si};
int siren[10] = {Do, Mi, Do, Mi, Do, Mi, Do, Mi, Do, Mi};
int beesong[14] = {So, Mi, Mi, Fa, Re, Re, Do, Do, Re, Mi, Fa, So, So, So};
int buzzerpin = 9;

// 麥克風腳位 https://reurl.cc/oL6LdM
int soundPin = A0;
int sensorValue = 0;

//PM2.5 sensor腳位 https://reurl.cc/Aqa1Vp
#define PIN_LED 5
#define PIN_OUTPUT A0
GP2Y1010AU0F GP2Y1010AU0F(PIN_LED, PIN_OUTPUT);

//超聲波sensor腳位 https://reurl.cc/D97965
#define TRIG_PIN    12          // trigger Pin
#define ECHO_PIN    11          // Echo Pin
#define MAX_DISTANCE 200        
// Maximum distance we want to ping for (in centimeters). 
// Maximum sensor distance is rated at 400-500cm. 

// 設定 NewPing 物件，並給與最遠測試距離
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);

defineTask(Task1);
    void Task1::setup()
    {
        Serial.begin(9600); //鮑率須確認
    }

    void Task1::loop()
    {
        // 麥克風程式，麥克風收音值超過500，播放救護車音效
        sensorValue = analogRead(soundPin);
        
        if(sensorValue > 500){
            for (int i = 0;i < 10; i++) {
            tone(buzzerpin, ambulance[i]);
            sleep(10);  
            }
            noTone(buzzerpin);//不發出聲音
        }
        
        else{
            sleep(1000);
        }
    }

defineTask(Task2);
    void Task2::setup()
    {
    }

    void Task2::loop()
    {
        //PM 2.5 sensor抓取空氣狀態輸出訊號，電壓值超過500，播放救護車音效
        double outputV = GP2Y1010AU0F.getOutputV(); //採樣獲取輸出電壓
        
        if(outputV > 1.50){
            for (int i = 0;i < 10; i++) {
            tone(buzzerpin, siren[i]);
            sleep(10);  
            }
            noTone(buzzerpin); //不發出聲音
        }
        
        else{
            sleep(1000);
        }
    }

defineTask(Task3);
    void Task3::setup()
    {
    }

    void Task3::loop()
    {
        //超聲波範例
        sleep(50);                        // 等待 50 毫秒
        unsigned int uS = sonar.ping();   // 送出 ping，並取得微秒 microseconds(uS) 時間
        int distance = sonar.convert_cm(uS); // 換算時間為距離

        if(distance < 15){
            for (int i = 0;i < 14; i++) {
            tone(buzzerpin, beesong[i]);
            sleep(100);  
            }
            noTone(buzzerpin); //不發出聲音
        }

        else{
            sleep(1000);
        }
    }

void setup()
{
    mySCoop.start();
}

void loop()
{
	yield();
}